"""Python multi-dispatcher library"""

__version__ = "0.5.2"

__author__ = "Dmitry Selyutin"
__copyright__ = "Copyright 2023 Dmitry Selyutin"
__license__ = "BSD"
__maintainer__ = "Dmitry Selyutin"
__email__ = "ghostmansd@gmail.com"
__status__ = "Prototype"
__all__ = [
    "dispatcher",
    "visitor",
    "walker",
]
